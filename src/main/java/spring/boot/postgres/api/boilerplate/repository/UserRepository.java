package spring.boot.postgres.api.boilerplate.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import spring.boot.postgres.api.boilerplate.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {}