package spring.boot.postgres.api.boilerplate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import spring.boot.postgres.api.boilerplate.model.User;
import spring.boot.postgres.api.boilerplate.repository.UserRepository;
import spring.boot.postgres.api.boilerplate.utils.PasswordChange;

@Controller
@RequestMapping(path="/boiler")

public class MainController {
	@Autowired 
	private UserRepository userRepository;

	@PostMapping(path="/user")
	public @ResponseBody ResponseEntity<User> addNewUser (@RequestBody User user) {
		
//		@query("select u from users u where u.username=" + user.getUsername())
		
		userRepository.save(user);
		return new ResponseEntity<>(user, HttpStatus.CREATED);
	}

	@GetMapping(path="/user")
	public @ResponseBody Iterable<User> getAllUsers() {
		return userRepository.findAll();
	}
	
	@GetMapping(path="/user/{id}")
	public ResponseEntity<User> read(@PathVariable("id") Long id) {
	    User userSearch = userRepository.findById(id).get();
	    if (userSearch == null) {
	        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	    } else {
	        return new ResponseEntity<>(userSearch, HttpStatus.OK);
	    }
	}
	
	@PutMapping(path="/user/{id}")
	public @ResponseBody ResponseEntity<User> updateUserById(@PathVariable long id, @RequestBody User user) {
		User existingUser = userRepository.findById(id).get();
				
		if  (existingUser != null) {
			existingUser.setUsername(user.getUsername());
			existingUser.setFirstname(user.getFirstname());
			existingUser.setLastname(user.getLastname());
			userRepository.save(existingUser);
			
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(path="/user/{id}/password")
	public @ResponseBody ResponseEntity<?> updateUserPassById(@PathVariable long id, @RequestBody PasswordChange passReq) {
		User existingUser = userRepository.findById(id).get();
		String dbPassword = existingUser.getPassword();
		String reqPassword = passReq.getOldPassword();
		String newPassword = passReq.getNewPassword();
		
		String errorMess = "Expected dbPassword: " + dbPassword + " but received reqPassword: " + reqPassword;
				
		if  (existingUser != null && reqPassword.equals(dbPassword)) {
			existingUser.setPassword(newPassword);
			userRepository.save(existingUser);
			
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		else if (existingUser != null && !reqPassword.equals(dbPassword)) {
			return new ResponseEntity<>(errorMess, HttpStatus.UNAUTHORIZED);
		}
		else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping(path="/user/{id}")
	public @ResponseBody ResponseEntity<User> deleteUserbyId(@PathVariable long id) {
		userRepository.deleteById(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}