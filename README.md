# Spring Boot API Boilerplate with PostgreSQL

#### If using Eclipse
`./gradlew eclipse` etc

#### To build
`./gradlew build`

##### DB Config
You must have a PGSQL database with the name of `testdb` available to your default `postgres` user.

You must then set the path variable for your `postgres` account in your IDE bound to the `PG_DB_PASSWORD` environment variable. 

### TODO:

-Check if user exists before creation
-Update user password endpoint - In progress
-change username endpoint?
-define username as unique in model
-Implement sementic versioning & api version path
-Swagger || OpenAPI Validation
-integrate BCrypt password hash + salt
-Enforce content type application json only
-error handling
-log4j/slf4j
-Dockerfile
-K8s
